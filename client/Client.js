'use strict'

const { PeerRPCClient, PeerRPCServer } = require('grenache-nodejs-http')
const Link = require('grenache-nodejs-link')

function Client (email) {
  this.email = email

  const link = new Link({
    grape: 'http://127.0.0.1:30001'
  })
  link.start()

  const peer = new PeerRPCClient(link, {})
  peer.init()

  this.peer = peer
  this.ping()
}

Client.prototype.ping = function () {
  this.request({ event: 'PING' })
}

Client.prototype.request = function (payload) {
  payload.signature = this.email
  return this.peer.request(
    'exchange_worker',
    payload,
    { timeout: 10000 },
    (err, data) => {
      if (err) {
        console.error(err)
        process.exit(-1)
      }
      console.log(this.email, data)
      return data
    }
  )
}

// Client.prototype.authenticate = function () {
//   return this.peer.request('auth', payload, { timeout: 10000 }, (err, data) => {
//     if (err) {
//       console.error(err)
//       process.exit(-1)
//     }
//     return data
//   })
// }

Client.prototype.newOrder = function (payload) {
  this.request(payload)
}

module.exports = Client
