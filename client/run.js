const Client = require('./Client')
const EVENTS = require('../constant/events')

const pair = {
  base: 'LEO',
  quote: 'USD'
}

function simulateTraderOne () {
  const one = new Client('clynch33@gmail.com')

  one.newOrder({
    event: EVENTS.ORDER_NEW,
    data: { type: 'MARKET', side: 'SELL', amount: 100, price: 1.23, pair }
  })

  one.newOrder({
    event: EVENTS.ORDER_NEW,
    data: { type: 'MARKET', side: 'BUY', amount: 100, price: 1.23, pair }
  })
}

function simulateTraderTwo () {
  const two = new Client('chad.lynch@bitfinex.com')
  two.newOrder({
    event: EVENTS.ORDER_NEW,
    data: { type: 'MARKET', side: 'BUY', amount: 100, price: 1.23, pair }
  })
}

simulateTraderOne()
simulateTraderTwo()
