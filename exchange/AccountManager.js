'use strict'

function AccountManager () {
  this.list = {}
}

AccountManager.prototype.setup = function (message) {
  if (!this.list[message.payload.signature]) {
    this.list[message.payload.signature] = {
      balances: { USD: 1000000, LEO: 100000 },
      orders: [],
      trades: []
    }
  }
}

AccountManager.prototype.available = function () {}

module.exports = AccountManager
