'use strict'

const OrderManager = require('./OrderManager')
const AccountManager = require('./AccountManager')

const EVENTS = require('../constant/events')

function Engine () {
  this.accounts = new AccountManager()

  this.orders = new OrderManager(this.accounts)
}

Engine.prototype.router = function (message) {
  this.accounts.setup(message)

  const {
    payload: { event },
    handler
  } = message

  switch (event) {
    case EVENTS.ORDER_NEW: {
      this.orders.place(message)
      break
    }
    case EVENTS.BOOK: {
      break
    }
    default: {
      handler.reply(null, EVENTS.INVALID)
      break
    }
  }
}

module.exports = Engine
