'use strict'

const OrderManager = require('./OrderManager')

function Matcher (accounts, book) {
  this.accounts = accounts
  this.book = book
}

Matcher.prototype.match = function (message) {
  const book = this.book
  const accounts = this.accounts

  const {
    payload: {
      signature,
      data: { side, amount, price }
    }
  } = message

  console.log('MATCH THIS', message)

  let unfilledAmount = amount

  const matchWorker = setInterval(() => {
    if (unfilledAmount) {
      clearInterval(matchWorker)
    }
    matchOrder(unfilledAmount)
  })

  function matchOrder (remainder, price) {
    console.log('LEFT', remainder)

    book[side].forEach((order, i) => {
      accounts.list[signature].balances.LEO += remainder
      accounts.list[signature].balances.USD += remainder
      unfilledAmount = unfilledAmount - remainder
      console.log(accounts.list[signature])
      book[side].splice(i, 1)
    })

    // update book
    // update taker
    // update maker
    // update accounts
    // update taker
    // update maker
  }
}

module.exports = Matcher
