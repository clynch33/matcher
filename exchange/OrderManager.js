'use strict'

const RESPONSES = require('../constant/responses')
const Matcher = require('./Matcher')

function OrderManager (accounts) {
  this.accounts = accounts
  this.book = {
    BUY: [],
    SELL: []
  }
  this.matcher = new Matcher(this.accounts, this.book)
}

OrderManager.prototype.place = function (message) {
  const {
    rid,
    payload: {
      signature,
      data: { type, side, amount, price, base, quote }
    }
  } = message

  const account = this.accounts.list[signature]

  // check if user has enough base
  if (account.balances[quote] < amount * price) {
    message.handler.reply(null, RESPONSES.INSUFFICIENT_BALANCE)
  }

  // add new order to the book
  this.book[side].push({ rid, signature, amount, price })

  this.matcher.match(message)

  message.handler.reply(null, RESPONSES.ORDER_PLACED)
}

module.exports = OrderManager
