'use strict'

const { PeerRPCServer } = require('grenache-nodejs-http')
const Link = require('grenache-nodejs-link')
const Engine = require('./Engine')

try {
  const link = new Link({
    grape: 'http://127.0.0.1:30001'
  })

  link.start()

  const peer = new PeerRPCServer(link, {
    timeout: 300000
  })
  peer.init()

  const port = 1024 + Math.floor(Math.random() * 1000)
  const service = peer.transport('server')
  service.listen(port)

  const engine = new Engine()

  setInterval(function () {
    link.announce('exchange_worker', service.port, {})
  }, 1000)

  service.on('request', (rid, key, payload, handler) => {
    /**
      { event, data, signature } payload structure
     */
    const { event, data, signature } = payload

    // handle PING
    if (event === 'PING') return handler.reply(null, 'PONG')

    // handle request
    if (event && data && signature)
      return engine.router({ rid, key, payload, handler })

    // catch invalid request payload
    return handler.reply(null, RESPONSES.INVALID_REQUEST)
  })
} catch (err) {
  console.log(err)
}
