const util = require('util')
const { execFile, exec, spawn } = require('child_process')
const { blueBright, green, red, yellow } = require('chalk')

async function execChild (string) {
  console.log(yellow(`[${Date.now()}]${' Grape'}:`), string)

  const child = await exec(string)
  child.stderr.on('data', console.log)
  child.stdout.on('data', console.log)
}

async function execFileProc (cmd, params, name) {
  const child = await execFile(cmd, params)
  child.stderr.on('data', data => console.log(red(`${name}:`, data.toString())))
  child.stdout.on('data', data => {
    const out = `[${Date.now()}] ${name}:`
    console.log(
      name === 'Server' ? blueBright(out) : green(out),
      data.toString()
    )
  })
}

async function start () {
  await execChild("grape --dp 20001 --aph 30001 --bn '127.0.0.1:20001'")
  await execChild("grape --dp 20001 --aph 30001 --bn '127.0.0.1:20002'")

  await execFileProc('node', ['./exchange/server.js'], 'Server')
  setTimeout(() => execFileProc('node', ['./client/run.js'], 'Client'), 1500)
}

start()
